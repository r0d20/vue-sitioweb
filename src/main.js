import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import rutas from './Rutas'

Vue.use(VueRouter);

const elRouter = new VueRouter(
  // {routes: routes}
    {routes: rutas}
);

Vue.config.productionTip = false;

new Vue({
  el: '#app',
  render: h => h(App),
  router: elRouter
});
