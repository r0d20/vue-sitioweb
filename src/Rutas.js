import inicio from './componentes/inicio.vue'
import quienesSomos from './componentes/quienesSomos.vue'
import servicios from './componentes/servicios.vue'
import contacto from './componentes/contacto.vue'

export default [ 
  {path: '/', component: inicio},
  {path: '/nosotros', component: quienesSomos},
  {path: '/servicios', component: servicios},
  {path: '/contacto', component: contacto}
  ];